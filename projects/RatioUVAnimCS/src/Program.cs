using System;
using System.Windows;
using System.Windows.Forms;
using System.Drawing;

class Program
{
	static void Main()
	{
		using(Form f = new MyForm())
		{
			Application.Run(f);
		}
	}

	class MyForm : Form
	{
		UVAnim d;

		public MyForm()
		{
			this.SuspendLayout();
			//instance
			{
				d = new UVAnim(this);

				//timer
				{
					Timer t = new Timer();
					t.Interval = (int)(1000 / 60);
					t.Enabled = true;
					t.Tick += new EventHandler(this.Tick);
				}
			}
			//design
			{
				this.FormBorderStyle = FormBorderStyle.FixedSingle;
				this.ClientSize = new Size(800, 600);
				this.DoubleBuffered = true;
				this.MaximizeBox = false;
				this.Text = "RatioUVAnimCS";
			}
			//load
			{
				d.Init();
			}
			this.ResumeLayout(false);
		}

		void Tick(object sender, EventArgs e)
		{
			d.Update();
			this.Invalidate();
			this.Update();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			d.Draw(e.Graphics);
		}
	}
	
	class UVAnim
	{
		Form _form;
		Form Form {get {return _form;}}

		Image img;

		int cnt0 = 0;
		int cnt1 = 0;

		public UVAnim(Form f)
		{
			this._form = f;
		}

		public void Init()
		{
			System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
			img = new Bitmap(asm.GetManifestResourceStream("img.png"));
		}

		public void Update()
		{
			if(cnt0 < 1)
			{
				cnt0++;
			}
			else
			{
				cnt0 = 0;
				cnt1++;
				cnt1 %= 100;
			}
		}

		public void Draw(Graphics g)
		{
			int u = cnt1 % 10;
			int v = cnt1 / 10;
			g.DrawImage(img, 0 - (u * 800), 0 - (v * 600), img.Width * 50, img.Height * 50);
		}
	}
}