package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author takumi
	 */
	public class ViewerBilinearVector extends ViewerBase
	{
		private var data:Vector.<Vector.<BitmapData>> = new Vector.<Vector.<BitmapData>>();
		
		private var loadI:int = 0;
		private var loadJ:int = 0;
		
		public function ViewerBilinearVector ()
		{
			loadMax = 25;
		}
		
		override public function load():Boolean 
		{
			var j:int = loadJ;
			var i:int = loadI;

			//
			{
				if (i == 0)
				{
					data.push(new Vector.<BitmapData>());
				}

				//add
				{
					data[j].push(new BitmapData(800, 600, false, 0xFF00FF00));
				}
				
				//create
				{
					var u:int = i;
					var v:int = j;
					drawImage2(data[v][u], u, v);
				}
			}
			//count
			{
				var func:Function = function ():void
				{
					loadValue = (loadJ * 5) + loadI;
				}
				loadI++;
				if (loadI < 5)
				{
					func();
					return false;
				}
				else
				{
					loadJ++;
					loadI = 0;
					if (loadJ < 5)
					{
						func();
						return false;
					}
					else
					{
						func();
						return true;
					}
				}
			}
		}
		
		override protected function drawImage(bd:BitmapData, u:int, v:int):void 
		{
			bd.draw(data[v][u]);
		}

		protected function drawImage2(bd:BitmapData, u:int, v:int):void 
		{
			var src:Vector.<uint> = img.bitmapData.getVector(new Rectangle(u * 16, v * 12, 16 + 1, 12 + 1));			
			var dist:Vector.<uint> = crateBilinearImage(src, 16, 12, 16 + 1, int(16 * scale), int(12 * scale));

			bd.setVector(new Rectangle(0, 0, 16 * scale, 12 * scale), dist);
		}
		
		private function getLinearColor(c0:uint, c1:uint, v:Number):uint
		{
			var a0:int = (c0 >> 24) & 0xFF;
			var r0:int = (c0 >> 16) & 0xFF;
			var g0:int = (c0 >> 8) & 0xFF;
			var b0:int = (c0 >> 0) & 0xFF;
			var a1:int = (c1 >> 24) & 0xFF;
			var r1:int = (c1 >> 16) & 0xFF;
			var g1:int = (c1 >> 8) & 0xFF;
			var b1:int = (c1 >> 0) & 0xFF;
			var a:int = int(a0 + int((a1 - a0) * v));
			var r:int = int(r0 + int((r1 - r0) * v));
			var g:int = int(g0 + int((g1 - g0) * v));
			var b:int = int(b0 + int((b1 - b0) * v));
			return uint((a << 24) | (r << 16) | (g << 8) | (b << 0));
		}
		
		private function crateBilinearImage(srcData:Vector.<uint>, srcWidth:int, srcHeight:int, srcStride:int, distWidth:int, distHeight:int): Vector.<uint>
		{

			var distData:Vector.<uint> = new Vector.<uint>;
			distData.length = distWidth * distHeight;

			for (var j:int = 0; j < distHeight; j++)
			{
				for (var i:int = 0; i < distWidth; i++)
				{
					var srcCurXf:Number = i * srcWidth / distWidth;
					var srcCurYf:Number = j * srcHeight / distHeight;
					var srcCurXi:int = int(srcCurXf);
					var srcCurYi:int = int(srcCurYf);
					var srcCurXv:Number = srcCurXf - srcCurXi;
					var srcCurYv:Number = srcCurYf - srcCurYi;
					distData[(j * distWidth) + i] = getLinearColor(
						getLinearColor(srcData[((srcCurYi + 0) * srcStride) + srcCurXi], srcData[((srcCurYi + 0) * srcStride) + srcCurXi + 1], srcCurXv),
						getLinearColor(srcData[((srcCurYi + 1) * srcStride) + srcCurXi], srcData[((srcCurYi + 1) * srcStride) + srcCurXi + 1], srcCurXv),
						srcCurYv
					);
				}
			}
			
			return distData;
		}
	}

}