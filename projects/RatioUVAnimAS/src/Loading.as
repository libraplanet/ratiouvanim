package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class Loading extends Sprite
	{
		private var text:TextField = new TextField();
		private var cnt:int = 0;
		public function Loading() 
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = 32;
			text.autoSize = TextFieldAutoSize.LEFT;
			text.selectable = false;
			text.defaultTextFormat = textFormat;
			text.x = 360;
			text.y = 240;
			text.textColor = 0xFFFFFFFF;
			text.text = "loading";

			addChild(text);
		}
		
		public function update(value:int, max:int):void
		{
			cnt++;
			cnt %= 10;
			
			text.text = "loading";
			for (var i:int = 0; i < cnt; i++)
			{
				text.text = text.text + ".";
			}
			text.text = text.text + "\n";
			text.text = text.text + "" + value + "/" + max;
		}
		
	}

}