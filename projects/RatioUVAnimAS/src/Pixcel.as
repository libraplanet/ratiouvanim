package  
{
	/**
	 * ...
	 * @author takumi
	 */
	public class Pixcel
	{
		public var c0:int = 0; //a
		public var c1:int = 0; //r
		public var c2:int = 0; //g
		public var c3:int = 0; //b

		public function Pixcel(c0:int = 0, c1:int = 0, c2:int = 0, c3:int = 0):void
		{
			this.c0 = c0;
			this.c1 = c1;
			this.c2 = c2;
			this.c3 = c3;
		}

		public function deepCloneAtPixcel():Pixcel
		{
			return new Pixcel(c0, c1, c2, c3);
		}

		public function add(p:Pixcel):void
		{
			this.c0 += p.c0;
			this.c1 += p.c1;
			this.c2 += p.c2;
			this.c3 += p.c3;
		}

		public function dis(p:Pixcel):void
		{
			this.c0 -= p.c0;
			this.c1 -= p.c1;
			this.c2 -= p.c2;
			this.c3 -= p.c3;
		}

		public static function getPixcel(p0:Pixcel, p1:Pixcel, v:Number): Pixcel
		{
			var ret:Pixcel = new Pixcel();
			var dist:Pixcel = p1.deepCloneAtPixcel();
			dist.dis(p0);
			
			ret.c0 = uint(p0.c0 + (dist.c0 * v));
			ret.c1 = uint(p0.c1 + (dist.c1 * v));
			ret.c2 = uint(p0.c2 + (dist.c2 * v));
			ret.c3 = uint(p0.c3 + (dist.c3 * v));
			return ret;
		}
	}
}