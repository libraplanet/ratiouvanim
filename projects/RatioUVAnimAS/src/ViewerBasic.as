package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class ViewerBasic extends ViewerBase
	{
		protected override function drawImage(bd:BitmapData, u:int, v:int):void
		{
			var m:Matrix = new Matrix();

			m.scale(scale, scale);
			m.translate( -(u * 800), -(v * 600));
			
			bd.draw(img, m, null, null, null, true);
		}
	}
}