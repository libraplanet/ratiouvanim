package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author takumi
	 */
	public class ViewerBase 
	{
		private var cnt0:int = 0;
		private var cnt1:int = 0;
		protected var scale:Number = 50;
		protected var img:Bitmap;
		
		public var loaded:Boolean = false;
		public var loadValue:int = 0;
		public var loadMax:int = 100;
		
		public function ViewerBase()
		{
		}
		
		public function init(IMG:Class):void
		{
			img = new IMG as Bitmap;
			img.smoothing = true;
		}
		
		public function load():Boolean
		{
			loadValue = loadMax = 100;
			return true;
		}
		
		public function getCnt():int
		{
			return cnt1;
		}
		
		public function update():void
		{
			if(cnt0 < 0)
			{
				cnt0++;
			}
			else
			{
				cnt0 = 0;
				cnt1++;
				cnt1 %= 25;
			}
		}
		
		public function draw(bd:BitmapData):void
		{
			drawImage(bd, cnt1 % 5, cnt1 / 5);
		}
		
		protected function drawImage(bd:BitmapData, u:int, v:int):void
		{
		}
	}
}