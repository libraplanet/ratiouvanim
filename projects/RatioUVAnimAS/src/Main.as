package 
{
	import adobe.utils.CustomActions;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;

	/**
	 * ...
	 * @author takumi
	 */
	public class Main extends Sprite 
	{
		[Embed(source = "../res/bg.png")]
		private const IMG_BG:Class;
		
		[Embed(source = "../res/img.png")]
		private const IMG_IMG:Class;
		
		//clipping用
		private var bdRoot:BitmapData = new BitmapData(800, 600);
		
		//private var viewer:ViewerBase = new ViewerBasic();
		//private var viewer:ViewerBase = new ViewerBilinearBA();
		private var viewer:ViewerBase = new ViewerBilinearVector();

		private var loading:Loading = new Loading();
		private var text:TextField = new TextField();
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			{
				stage.quality = StageQuality.BEST;

				viewer.init(IMG_IMG);

				//text
				{
					var textFormat:TextFormat = new TextFormat();
					textFormat.size = 32;
					text.autoSize = TextFieldAutoSize.LEFT;
					text.selectable = false;
					text.defaultTextFormat = textFormat;
					text.x = 0;
					text.y = 0;
					text.textColor = 0x6666666;
					text.text = "loading";
				}
				
				//add
				{
					addChild(new Bitmap(bdRoot));
					//addChild(text);
						
					addEventListener(Event.ENTER_FRAME, tick);
				}
			}				
		}
		
		/**
		 * tick
		 * @param	event
		 */
		private function tick(event:Event):void
		{
			if (!viewer.loaded)
			{
				//update
				{
					viewer.loaded = viewer.load();
					loading.update(viewer.loadValue, viewer.loadMax);
				}
				//draw
				{
					bdRoot.draw(new IMG_BG as Bitmap);
					bdRoot.draw(loading);
				}
			}
			else
			{
				//update
				{
					viewer.update();
					
					text.text = "" + viewer.getCnt();
				}
				
				//draw
				{
					viewer.draw(bdRoot);
				}
			}
		}
	}
}