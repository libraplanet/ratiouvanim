package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import org.flashdevelop.utils.FlashConnect;
	
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class ViewerBilinearBA extends ViewerBase 
	{
		private var img:Bitmap;
		private var data:Vector.<Vector.<BitmapData>> = new Vector.<Vector.<BitmapData>>();
		
		public function ViewerBilinearBA() 
		{
		}

		private const MAX:int = 1;
		public override function init(IMG:Class):void
		{
			super.init(IMG);

			for (var j:int = 0; j < 5; j++ )
			{
				data.push(new Vector.<BitmapData>());
				for (var i:int = 0; i < 5; i++ )
				{
					//add
					{
						data[j].push(new BitmapData(800, 600, false, 0xFF00FF00));
					}
					
					//create
					if(((j * 5) + i) < MAX)
					{
						var u:int = i;
						var v:int = j;
						drawImage2(data[v][u], u, v);
					}
				}				
			}
		}
		
		protected override function drawImage(bd:BitmapData, u:int, v:int):void
		{
			//bd.draw(data[v][u]);
			bd.draw(data[0][u % MAX]);
		}

		protected function drawImage2(bd:BitmapData, u:int, v:int):void
		{
			var src:ByteArray = img.bitmapData.getPixels(new Rectangle(u * 16, v * 12, 16 + 1, 12 + 1));
			var dist:ByteArray = crateBilinearImage2(src, 16, 12, 16 + 1, scale, scale);

			dist.position = 0;
			
			//bd.setPixels(new Rectangle(0, 0, 16, 12), src);
			bd.setPixels(new Rectangle(0, 0, 16 * scale, 12 * scale), dist);
			//bd.draw(img);
		}
		private function crateBilinearImage2(srcData:ByteArray, srcWidth:int, srcHeight:int, stride:int, scaleX:Number, scaleY:Number): ByteArray
		{
			var src:Vector.<int> = new Vector.<int>();
			srcData.position = 0;
			while (srcData.position < srcData.length)
			{
				src.push(srcData.readByte());
			}
			return crateBilinearImage(src, srcWidth, srcHeight, stride, scaleX, scaleY);
		}
		
		private function crateBilinearImage(srcData:Vector.<int>, srcWidth:int, srcHeight:int, stride:int, scaleX:Number, scaleY:Number): ByteArray
		{
			var distData:ByteArray = new ByteArray();
			var distWidth:int = srcWidth * scaleX;
			var distHeight:int = srcHeight * scaleY;
			
			//srcData.
			
			for (var j:int = 0; j < distHeight; j++)
			{
				for (var i:int = 0; i < distWidth; i++)
				{
					//
					if(true)
					{
						var curXf:Number = i * srcWidth / Number(distWidth);
						var curYf:Number = j * srcHeight / Number(distHeight);
						var curXi:int = int(curXf);
						var curYi:int = int(curYf);
						var pix:Pixcel;
						{
							var i0:int;
							var i1:int;
							var p0:Pixcel;
							var p1:Pixcel;
							var pixV:Pixcel;
							var pixH:Pixcel;
							//v
							{
								i0 = (((curYi + 0) * stride) + (curXi + 0)) * 4;
								i1 = (((curYi + 0) * stride) + (curXi + 1)) * 4;
								p0 = new Pixcel(srcData[i0 + 0], srcData[i0 + 1], srcData[i0 + 2], srcData[i0 + 3]);
								p1 = new Pixcel(srcData[i1 + 0], srcData[i1 + 1], srcData[i1 + 2], srcData[i1 + 3]);
								pixV = Pixcel.getPixcel(p0, p1, curXf - curXi);
								FlashConnect.trace("[" + j + "][" + i + "]V curXf(" + curXf + ") + curXi(" + curXi + ") + v(" + (curXi - int(curXf)) + ")");
							}
							//h
							{
								i0 = (((curYi + 0) * stride) + (curXi + 0)) * 4;
								i1 = (((curYi + 1) * stride) + (curXi + 0)) * 4;
								p0 = new Pixcel(srcData[i0 + 0], srcData[i0 + 1], srcData[i0 + 2], srcData[i0 + 3]);
								p1 = new Pixcel(srcData[i1 + 0], srcData[i1 + 1], srcData[i1 + 2], srcData[i1 + 3]);
								pixH = Pixcel.getPixcel(p0, p1, curYf - curYi);
								FlashConnect.trace("[" + j + "][" + i + "]H curYf(" + curYf + ") + curYi(" + curYi + ") + v(" + (curYf - curYi) + ")");
							}
							pix = Pixcel.getPixcel(pixV, pixH, 0.5);
						}
						

						if (true)
						{
							distData.writeByte(int(pix.c0));
							distData.writeByte(int(pix.c1));
							distData.writeByte(int(pix.c2));
							distData.writeByte(int(pix.c3));
						}
						else
						{
							distData.writeByte(255);
							distData.writeByte(0);
							distData.writeByte(0);
							distData.writeByte(255);
						}
					}
				}				
			}
			
			return distData;
		}
	}
}